from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
from .models import Proyectos as proyectos_model
from .models import Configuracion as configuracion_model
# Create your views here.

def index_view(request):
    proyectos_list = proyectos_model.objects.all()
    configuracion = configuracion_model.objects.first()
    context = {
        'proyectos_list': proyectos_list,
        'configuracion': configuracion
    }
    return render(request, 'index_templates/index.html', context)