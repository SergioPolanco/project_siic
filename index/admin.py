from django.contrib import admin

# Register your models here.
from .models import Proyectos, Configuracion

admin.site.register(Proyectos)
admin.site.register(Configuracion)