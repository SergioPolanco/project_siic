from __future__ import unicode_literals

from django.db import models

# Create your models here.
import os

def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)

class Proyectos(models.Model):
    class Meta():
        db_table = "proyectos"
    
    titulo_proyecto = models.CharField(max_length = 200, null = False)
    tipo_proyecto = models.CharField(max_length = 200, null= False)
    descripcion = models.CharField(max_length= 1000, null= False)
    image = models.ImageField(upload_to=get_image_path, blank=True, null=True)

class Configuracion(models.Model):
    class Meta():
        db_table = "configuracion"
        
    mision = models.TextField()
    vision = models.TextField()
    telefono = models.CharField(max_length= 50)
    direccion = models.TextField()
    ruc = models.CharField(max_length=50)
    quienes_somos = models.TextField()
    resena_proyectos = models.TextField()
    resena_servicios = models.TextField()